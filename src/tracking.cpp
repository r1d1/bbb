/*!
\file      r1d1.cpp

\note:    The serial program is inspired by the work of Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\note:    The tracking code is inspired from https://code.ros.org/trac/opencv/browser/trunk/opencv/samples/cpp/camshiftdemo.cpp?rev=4234
\author   jnanar
I place this piece of code in the public domain. Feel free to use as you see
fit. I'd appreciate it if you keep my name at the top of the code somehwere,
but whatever.
Main project site: https://gitlab.com/groups/r1d1
\version   0.1
\date      11/04/2015
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/



#include "common.h"
#include "depend.hpp"
#include "utils.hpp"

using namespace std;
using namespace cv;

void boum_tracking(Rect& selection, int& trackObject)
    {
    selection.width = 0;
    selection.height = 0;
    selection.x = 0;
    selection.y = 0;
    trackObject = 0;
    }

void reaction(Mat&image, RotatedRect trackBox, int xmed, int ymed,
              const double scale, bool comok, serialib& LS )
    {
    /*   if (debug_mode == 1){
    cout << "Tracking " << trackBox.center.x << " - "<<  trackBox.center.y
    << trackBox.size.width << " - " <<trackBox.size.height << " - "
    <<  trackBox.angle << " - " <<trackBox.size.height * trackBox.size.width<< endl;
    }
    */
    Rect brect = trackBox.boundingRect();
    rectangle(image, brect, Scalar(255,0,0),2,8);

    vector<int> res(6);
    res.at(0) = brect.x;
    res.at(1) = brect.width;
    res.at(2) = brect.y;
    res.at(3) = brect.height;
    res.at(4) = brect.width * brect.height;
    res.at(5) = 1 ;

    // This doesn't work
    /*    res.at(0) = trackBox.center.x;
    res.at(1) = trackBox.center.y;
    res.at(2) =trackBox.size.width;
    res.at(3) = trackBox.size.height;
    res.at(4) =trackBox.size.width * trackBox.size.height;
    res.at(5) = 1 ;
    */
    brain(res, xmed, scale ,comok,LS, "tracking");

    }

long int seek_and_destroy(Mat& image, Mat&hsv, Mat&hist, Mat&histimg,
                          Mat&backproj,
                          Rect&trackWindow, int hsize,  float* hranges, int& trackObject,
                          Rect& selection, int vmin, int vmax, int smin, const double scale,
                          bool comok, bool win, serialib& LS)
    {
    time_t timer1 = 0;
    bool backprojMode = false;
    const float* phranges = hranges;
    RotatedRect trackBox;

    Mat hue, mask;

    cvtColor(image, hsv, CV_BGR2HSV);

    // if trackObject != 0
    if( trackObject )
            {
            int _vmin = vmin, _vmax = vmax;
            /// Checks if array elements lie between the elements of two other arrays.
            /// void inRange(InputArray src, InputArray lowerb, InputArray upperb, OutputArray dst)
            /// lowerb – inclusive lower boundary array or a scalar.
            /// upperb – inclusive upper boundary array or a scalar.
            /// dst – output array of the same size as src and CV_8U type.

            inRange(hsv, Scalar(0, smin, MIN(_vmin,_vmax)),
                    Scalar(180, 256, MAX(_vmin, _vmax)), mask);

            int ch[] = {0, 0};
            hue.create(hsv.size(), hsv.depth());
            // mixChannels: Copies specified channels from input arrays to the specified channels of output arrays.
            mixChannels(&hsv, 1, &hue, 1, ch, 1);

            /*
                    cout << "S " << selection.width << " + " << selection.height <<  " - " ;
                    cout << "fr " << (int)image.at < unsigned char > (1,6) << " - " ;
                    cout << "hue " <<(int)hue.at < unsigned char > (1,1) << " - ";
                    cout << "mask " <<(int)mask.at < unsigned char > (1,1) << " - ";
                    cout << "hsv " <<(int)hsv.at < unsigned char > (1,1) << endl;
            */
            // The evaluation order is specified by the standard and is left-to-right.
            // TODO !!!!!!!!!!!!!!
            if(trackObject   < 0 &&
                    selection.x + selection.width <= hue.cols &&
                    selection.y + selection.height <= hue.rows &&
                    selection.width > 0 && selection.height > 0 ) // prevent errors
                    {
                    /*
                                cout << "x " << selection.x << "; y " << selection.y
                                << "; w " <<  selection.width << "; h " << selection.height
                                << "; x+w " << selection.x + selection.width << "; y+h " <<  selection.y + selection.height
                                << endl;
                    */


                    try
                            {
                            // BUG HERE SOMETIMES
                            // Because,
                            // Assertion failed (0 <= roi.x && 0 <= roi.width && roi.x + roi.width <= m.cols && 0 <= roi.y &&
                            // 0 <= roi.height && roi.y + roi.height <= m.rows) in Mat, file matrix.cpp, line 323

                            Mat roi(hue, selection);
                            Mat maskroi(mask, selection);
                            //Mat roi(hue, selection), maskroi(mask, selection);
                            // Calculates a histogram of a set of arrays.
                            calcHist(&roi, 1, 0, maskroi, hist, 1, &hsize, &phranges);
                            normalize(hist, hist, 0, 255, CV_MINMAX);
                            // rectangle de la selection
                            trackWindow = selection;
                            // on a un object à traquer
                            trackObject = 1;
                            histimg = Scalar::all(0);
                            int binW = histimg.cols / hsize;
                            Mat buf(1, hsize, CV_8UC3);
                            for( int i = 0; i < hsize; i++ )
                                    {
                                    buf.at<Vec3b>(i) = Vec3b(saturate_cast<uchar>(i*180./hsize), 255, 255);
                                    }
                            cvtColor(buf, buf, CV_HSV2BGR);


                            for( int i = 0; i < hsize; i++ )
                                    {
                                    int val = saturate_cast<int>(hist.at<float>(i)*histimg.rows/255);
                                    rectangle( histimg, Point(i*binW,histimg.rows),
                                               Point((i+1)*binW,histimg.rows - val),
                                               Scalar(buf.at<Vec3b>(i)), -1, 8 );
                                    }
                            }
                    catch( cv::Exception& e )
                            {
                            const char* err_msg = e.what();
                            log(err_msg, 1);
                            //cout << "exception caught: " << err_msg << std::endl;
                            cout << "exception caught: " << "Matrix Assertion failed" << std::endl;
                            boum_tracking(selection, trackObject);
                            return -1;
                            }
                    }
            calcBackProject(&hue, 1, 0, hist, backproj, &phranges);
            backproj &= mask;
            if (trackWindow.width > 0 && trackWindow.height> 0)
                    {
                    trackBox = CamShift(backproj, trackWindow,
                                        TermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ));
                    }
            if( trackWindow.area() <= 1 )
                    {
                    int cols = backproj.cols, rows = backproj.rows, r = (MIN(cols, rows) + 5)/6;
                    trackWindow = Rect(trackWindow.x - r, trackWindow.y - r,
                                       trackWindow.x + r, trackWindow.y + r) &
                                  Rect(0, 0, cols, rows);
                    }

            if( backprojMode )
                    {
                    cvtColor( backproj, image, CV_GRAY2BGR );
                    }
            //draw the result
            // sometimes the edge of ellipse move but the center remains at the same place.
            // if nothing moves, tracking mode must be avorted
            if (trackBox.size.width > 0 && trackBox.size.height > 0)
                    {
                    ellipse( image, trackBox, Scalar(0,0,255), 3, CV_AA );
                    // we have something, time to log and tell the arduino.
                    timer1 = time(0);
                    if (comok == true )
                            {
                            reaction(image, trackBox, image.cols/2, image.rows/2, scale, comok, LS);
                            }
                    }
            }
    if(( trackBox.size.width < 1 && trackBox.size.height < 1) )
            {
            //fail
            boum_tracking(selection, trackObject);
            if(win==true)
                    {
                    destroyWindow( "Histogram" );
                    }
            }
    if(win == true)
            {
            imshow( "result", image );
            imshow( "Histogram", histimg );
            }
    return timer1;
    }


