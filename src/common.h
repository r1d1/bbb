/*
\file      common.h

\note:    The serial program is inspired by the work of Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\note:    The tracking code is inspired from https://code.ros.org/trac/opencv/browser/trunk/opencv/samples/cpp/camshiftdemo.cpp?rev=4234
\author   jnanar
I place this piece of code in the public domain. Feel free to use as you see
fit. I'd appreciate it if you keep my name at the top of the code somehwere,
but whatever.
Main project site: https://gitlab.com/groups/r1d1
\version   0.1
\date      11/04/2015
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/


// common global variable
extern int global;
extern int facedetect; // Global var bouuuuuh
extern int mode;
extern int debug_mode;
extern int prev_ac;  // previous face size
extern int checkpoint;
//extern serialib LS;

#if defined (_WIN32) || defined( _WIN64)
#define         DEVICE_PORT             "COM1"                               // COM1 for windows
#endif

// "/dev/ttyO1" or "/dev//dev/ttyVirtualS0"

#ifdef __linux__
#define         DEVICE_PORT             "/dev/ttyVirtualS0"         // ttyVirtualS0 for archlinux
//#define         DEVICE_PORT             "/dev/ttyO1"              // tty01 for linux
#endif
