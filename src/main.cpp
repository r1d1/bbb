/*!
\file      r1d1.cpp

\note:    The serial program is inspired by the work of Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\note:    The tracking code is inspired from https://code.ros.org/trac/opencv/browser/trunk/opencv/samples/cpp/camshiftdemo.cpp?rev=4234
\author   jnanar
I place this piece of code in the public domain. Feel free to use as you see
fit. I'd appreciate it if you keep my name at the top of the code somehwere,
but whatever.
Main project site: https://gitlab.com/groups/r1d1

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
\version   0.1
\date      11/04/2015
*/

#include <iostream>
#include "depend.hpp"
#include "common.h"
#include "utils.hpp"
#include "detect.hpp"
#include "lib/INIReader.h"
#include "tracking.hpp"


using namespace std;
using namespace cv;



int facedetect = 0; // Global var bouuuuuh
int mode = 0;
int debug_mode = 1;
int prev_ac = 0;  // previous face size
int checkpoint = 1;



void rec_movie(bool mvie, VideoWriter& video, Mat& frame, int res_w, int res_h)
    {
    if (mvie  == true)
            {
            video.open("out.avi",CV_FOURCC('M','J','P','G'),10, Size(res_w,res_h),true);
            }
    else
            {
            video.write(frame);
            }

    }


//_____________________________________________________________________________________________________________
int camera(int cam_num, bool win, bool scom, bool movie, int res_w, int res_h,
           serialib& LS)
    {
    bool comok;
    if (scom == true)
            {
            // Open serial port
            int Ret;                                                                // Used for return values
            Ret=LS.Open(DEVICE_PORT,
                        115200);                                        // Open serial link at 115200 bauds
            if (Ret!=1)                                                             // If an error occured...
                    {
                    cout <<  "Error while opening port. Permission problem ?\n"
                         <<endl;// ... display a message ...
                    cout << "Error on port " << DEVICE_PORT << endl;
                    log("Error while opening port. Permission problem ?\n ",0);
                    log("DEVICE_PORT",1);
                    comok = false;
                    return Ret;                                                         // ... quit the application
                    }
            else
                    {
                    log("Serial port opened successfully!\n",1);
                    comok = true;  // communications ok
                    //      char cmd[3];
                    //      strcpy(cmd, "OK");
                    //      puppet(cmd,comok, LS);

                    }
            }
    else
            {
            // tests sans serial
            comok = false;
            log("Test without serial possible !\n",1);
            }

    //1.0 for 1:1 processing. use bigger number to increase the speed
    //used as =>   smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    const double scale = 2.0;
    VideoCapture cap(cam_num);
    //VideoCapture cap("bbb.avi");
    cap.set(CV_CAP_PROP_FRAME_WIDTH,res_w);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,res_h);

    //change the path to the your haar file
    static String haarcascade_frontal_face = "./cascade/lbpcascade_frontalface.xml";
    static String haarcascade_palm = "./cascade/palm.xml";  //p palm.xml
    static String haarcascade_fist = "./cascade/fist.xml";
    static String hand = "paml.xml";
    static String fist = "fist.xml";

    CascadeClassifier cascade_face , cascade_palm , cascade_fist;

    //load cascade file for face detection
    //If the load function returns false, set a break point before this call
    //and see the contents inside "haarcascade_face".
    //If it loading the contents of Haar cascade, but returning false, then check your additional dependency.
    //If you are running under "debug", you have to use xxxxd.lib instead of xxxx.lib.
    if(!cascade_face.load(haarcascade_frontal_face))
            {
            cerr << "ERROR: Could not load classifier cascade face" << endl;
            return 1;
            }
    //load cascade file for palm detection
    if(!cascade_palm.load(haarcascade_palm))
            {
            cerr << "ERROR: Could not load classifier cascade palm" << endl;
            return 1;
            }
    //load cascade file for palm detection
    if(!cascade_fist.load(haarcascade_fist))
            {
            cerr << "ERROR: Could not load classifier cascade fist" << endl;
            return 1;
            }

    if (win == true)
            {
            //create window ARNAUD
            namedWindow("result", 1);  //arg2 = 1 for autosize
            namedWindow( "Histogram", 0 );
            }

    //process captured image
    log("In capture ...",1);
    time_t timer1, timer2;
    timer1 = time(0);   // this is the last time a face was detected
    //    long int *dd[2];
    VideoWriter video;
    Mat frame;
    cap.read(frame);
    if (movie == true)
            {
            rec_movie(true, video, frame, res_w, res_h );
            }

    // Tracking function
    Rect trackWindow;
    int trackObject = 0; // needed to enter the tracking condition
    int hsize = 16;
    float hranges[] = {0,180};
    Rect selection;
    int vmin = 10, vmax = 100, smin = 80;
    Mat hsv, hist, histimg = Mat::zeros(200, 320, CV_8UC3),  backproj;


    while( cap.isOpened() )
            {
            if ( ! cap.read(frame) )
                    {
                    break;
                    }
            // timer1 = time of detection


            if( selection.width > 1 && selection.height > 1 )
                    {
                    timer2 = seek_and_destroy(frame, hsv,  hist, histimg,backproj,
                                              trackWindow, hsize,  hranges, trackObject,
                                              selection, vmin, vmax, smin, scale,comok, win,LS);
                    if (timer2 > 0)
                            {
                            timer1 = timer2;
                            }
                    }

            else
                    {
                    timer1 = detectAndDraw( frame, cascade_face, cascade_palm,
                                            cascade_fist,scale, comok, win, timer1,LS, selection, trackObject);

                    }


            if (movie  == true)
                    {
                    // Save frame
                    rec_movie(false, video, frame, res_w, res_h );
                    }

            //Don't know why it needs wait call, but it always here...
            if( waitKey( 10 ) >= 0 )     //wait for 10msec before processing next frame
                    {
                    break;
                    }//if the key entry is detected, it will break from the loop

            }
    return 0;
    }




int main()
    {
    INIReader reader("r1d1.ini");
    if (reader.ParseError() < 0)
            {
            cout << "Can't load 'r1d1.ini'\n";
            return 1;
            }


    int res_w, res_h;
    res_w = reader.GetInteger("camera", "res_w", -1);
    res_h = reader.GetInteger("camera", "res_h", -1);
    int camera_num;
    camera_num = reader.GetInteger("camera", "camera", -1);

    bool win, scom, movie;
    win = reader.GetBoolean("config", "window", false);
    scom = reader.GetBoolean("config", "serial", false) ;
    debug_mode = reader.GetBoolean("config", "debug", false) ;
    movie = reader.GetBoolean("camera", "movie", false);




    cout << "Config loaded from 'r1d1.ini': res_w="
         << res_w << ", res_h="
         <<  res_h << ", window="
         << win << ", serial="
         << scom << ", camera="
         << camera_num <<  ", debug="
         << debug_mode << ", movie="
         << movie << "\n";


    serialib LS;         // Object of the serialib class
    camera(camera_num, win, scom, movie,res_w, res_h, LS );
    LS.Close();
    return 0;
    }



