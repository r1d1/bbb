/* This file was automatically generated.  Do not edit! */
long int seek_and_destroy(cv::Mat& image, cv::Mat&hsv, cv::Mat&hist,
                          cv::Mat&histimg, cv::Mat&backproj,
                          cv::Rect&trackWindow, int hsize,  float* hranges,
                          int& trackObject,
                          cv::Rect& selection, int vmin, int vmax,
                          int smin, const double scale, bool comok, bool win, serialib& LS);
