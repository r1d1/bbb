/*!
 \file      r1d1.cpp

\note:    The serial program is inspired by the work of Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\note:    The tracking code is inspired from https://code.ros.org/trac/opencv/browser/trunk/opencv/samples/cpp/camshiftdemo.cpp?rev=4234
\author   jnanar
I place this piece of code in the public domain. Feel free to use as you see
fit. I'd appreciate it if you keep my name at the top of the code somehwere,
but whatever.
Main project site: https://gitlab.com/groups/r1d1
\version   0.1
\date      11/04/2015
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "depend.hpp"
#include "common.h"
#include "utils.hpp"

using namespace std;
using namespace cv;

//
//          Dictionnary serial:
//          f = entrer mode detection de visage
//          a = avance
//          r = recule
//          g = gauche
//          d = droite
//          G = big turn to left
//          D = big turn to right
//          e = exploration
//          j = joyeux (buzzer)
//          t = triste (buzzer)
//          p = frustré (buzzer)
//          h = horreur - peur (buzzer)
//          f = detection visage (buzzer)
//          m = Changer de mode (vers detection visage ou vers explo)
//          ?? = Initialisation (rien ne se passe)
//          s  = melodie (buzzer)
//          w  = smoke on the water (buzzer)
//          x  = frustré 2 (buzzer rapide)
//          y  = modem (buzzer rapide comme une connection 56k)
//          z  = mystere (buzzer)
//          1  = delay (1 seconde)
//          2  = delay (2 secondes) en string


void log( const string &text, bool carriage )
    {
    std::ofstream log_file(
        "log.txt", std::ios_base::out | std::ios_base::app );
    if (carriage == 1)
            {
            log_file << text << endl;
            }
    else
            {
            log_file << text ;
            }
    }

void log_int( const int &text , bool carriage)
    {
    std::ofstream log_file(
        "log.txt", std::ios_base::out | std::ios_base::app );
    if (carriage == 1)
            {
            log_file << text << endl;
            }
    else
            {
            log_file << text ;
            }
    }



void log_serial( const string &text )
    {
    std::ofstream log_file(
        "serial.txt", std::ios_base::out | std::ios_base::app );
    log_file << text << endl;
    }

// read a string from the buffer
int listen(bool comok,serialib& LS, int to )
    {

    int Ret = 0;   // Used for return values
    if (comok == true)
            {
            int buffer_size = 128;
            char Buffer[128];
            // Read a string from the serial device
            // normally \n in the example,doest'nt work
            // it works with a final char different, i.e. '%'
            Ret=LS.ReadString(Buffer,'\r',buffer_size,to); // \r == carriage return
            // Read a maximum of 128 characters with a timeout of 'to' milliseconds
            // The final character of the string must be a carriage return ('\r')
            // this is achieve by using serial.println() on arduino
            std::string str(Buffer);
            if (debug_mode == 1)
                    {
                    cout << "Ret = " << Ret << endl;
                    }
            if (Ret>0)        // If a string has been read from, print the string
                    {
                    log_serial(Buffer);
                    if (debug_mode == 1)
                            {
                            cout << "String read from serial port : " << Buffer << endl;
                            }
                    // If the Buffer contain a "%", then the arduino is on detectface mode.
                    size_t found = str.find("%");
                    if (found!=string::npos)
                            {
                            // Ret is artificially bigger, we can return it.
                            Ret += 10000;
                            }
                    }
            else
                    {
                    if (debug_mode == 1)
                            {
                            cout << "TimeOut reached. No data received !" << endl ;
                            } // If not, print a message.
                    }
            //  cout << "buffer qd meme : " << Buffer << endl;
            //  cout << "##############""" << endl;
            }
    return Ret ;
    }


bool serialevent(bool comok, serialib& LS)
    {
    bool roger = false;
    int Ret = 1;
    while (Ret > 0)
            {
            Ret = listen(comok,LS, 100 );
            if (Ret > 10000)
                    {
                    roger = true;
                    }
            }

    return roger;
    }

bool puppet(char* cmd, bool comok,serialib& LS )
    {

    bool com = false;
    if (comok == true)
            {
            string msg;
            string start;
            int Ret;                                                      // Used for return values
            com = false;
            string s2, s1;
            std::string str(cmd); // cmd is a char pointer terminated by \0
            s1 = cmd;
            Ret=LS.WriteString(cmd);   // Send the command on the serial port Ret = 1 if ok
            if (Ret == 1)
                    {
                    com = true ;
                    }
            }
    return com;
    }

void brain(Vector<int> res, int xmed, double scale, bool comok,serialib& LS,
           const string mode_brain  )
    {
    int centerX, centerY, left_edge, right_edge, up_edge, down_edge, ac;
    // Define the image: center and borders
    if (mode_brain == "detect")
            {
            centerX = res[1]/2;
            centerX += res[0];
            centerY = res[3]/2;
            centerY += res[2];
            left_edge = res[0] ;
            right_edge = res[0] + res[1];
            up_edge = res[2] ;
            down_edge = res[2] + res[3];
            left_edge*=scale;
            right_edge*=scale;
            up_edge*=scale;
            down_edge*=scale;
            centerX *=scale; // reapply the scale factor.
            centerY *=scale;
            //before: res[1]*res[3]*scale*scale
            ac = res[4] *scale * scale;
            }
    else
        if (mode_brain == "tracking")
                {
                if (debug_mode == 1)
                        {
                        cout << "tracking " ;
                        }
                centerX = res[0];
                centerY = res[3];
                // not so correct, better if using a rectangle
                left_edge = res[0] - res[1]/2 ;
                right_edge = res[0] + res[1]/2;
                up_edge = res[2] + res[3]/2 ;
                down_edge = res[2] - res[3]/2 ;
                ac = res[4] ;
                }


    char cmd1[3];  // 2 possibilities= go left or right and go forward or backward
    char cmd2[3], cmd3[3], cmd4[3];  // 3 for the terminating null character

    strcpy(cmd1, "  ");
    strcpy(cmd2, "  ");
    strcpy(cmd3, "  ");
    strcpy(cmd4, "  ");
    bool talk1 = false;
    bool talk2 = false;
    bool talk3 = false;
    bool talk4 = false;


    int lim_right = xmed * 0.6;
    int lim_left = xmed * 1.4;


    if (debug_mode == 1)
            {
            cout << "centerX" << " " << "centerY" << endl ;
            cout << centerX << " " << centerY << endl ;

            cout << "left_edge"<< " "<< "right_edge" << endl ;
            cout << left_edge<< " "<< right_edge << endl ;
            cout << "up_edge"<< " "<< "down_edge" << endl ;
            cout << up_edge<< " "<< down_edge << endl ;
            cout << "Area"<< " "<< ac << endl ;
            }




    // array are not asignable.
    // use strcpy

    if (right_edge <  lim_right && centerX != 0)
            {
            strcpy(cmd1, "GG"); // long turn to left
            std::ofstream log_file(
                "log.txt", std::ios_base::out | std::ios_base::app );
            log_file << "long turn left " << "Center X - width/2 = " <<centerX  -
                     res[1]/2<< endl;
            log("Left",1);
            talk1 = true;
            }

    else
        if (left_edge > lim_left )
                {
                strcpy(cmd1, "DD"); // long turn to right
                std::ofstream log_file(
                    "log.txt", std::ios_base::out | std::ios_base::app );
                log_file << "long turn right " << "Center X + width/2 = " <<centerX  +
                         res[1]/2<< endl;
                log("Left",1);
                talk1 = true;

                }

        else
            if (right_edge < xmed && centerX != 0)   // tests position head % center ??????
                    {
                    strcpy(cmd1, "gg");
                    std::ofstream log_file(
                        "log.txt", std::ios_base::out | std::ios_base::app );
                    log_file << "Center X" <<centerX << endl;
                    log("Left",1);
                    talk1 = true;

                    }
            else
                if (left_edge >  xmed )   // tests position head % center
                        {
                        strcpy(cmd1, "dd");
                        log("Right",1);
                        std::ofstream log_file(
                            "log.txt", std::ios_base::out | std::ios_base::app );
                        log_file << "Center X" <<centerX << endl;
                        talk1 = true;

                        }


    if (ac < 0.05*prev_ac && ac != 0)
            {
            strcpy(cmd2, "aa");
            log("Forward ",0);
            log(to_string(ac) + "  " + to_string(prev_ac),1);
            talk2 = true;
            }
    else
        if (ac > 20.0 *prev_ac && prev_ac!= 0)
                {
                strcpy(cmd2, "rr");
                log("backward",1);
                talk2 = true;
                }

    // area size for 320x240 resolution make it dynamic depending on resolution
    if ( ac < 100 && ac != 0)   // tests position head % center
            {
            strcpy(cmd2, "??"); // Play a random song
            log("random song ",1);
            cmd1[0] = '\0';
            strcpy(cmd1, "aa");
            log("Forward",1);
            talk2 = true;
            talk1 = true;
            ac = 0;
            prev_ac = 0;
            }
    else
        if (ac  > 20000 )   // tests position head % center
                {
                strcpy(cmd1, "rr");
                log("Back",1);
                //   strcpy(cmd2, "??");
                //    log("Back");
                log("face too close, i am afraid",1);
                //    cout << "aah" ;
                //    sleep(0.5);
                strcpy(cmd2, "hh"); // lettre h pour horreur
                talk1 = true;
                talk2 = true;
                ac = 0;
                prev_ac = 0;
                //  ______________________________________________
                //  Afraid behaviour
                log("turn 180°",1);
                strcpy(cmd1, "GG");
                talk1 = true;
                log("explo a few seconds and go away ",0);
                strcpy(cmd2, "ee");
                log("Go back to detect mode",0 );
                strcpy(cmd3, "mm");
                log("turn 180°",1);
                strcpy(cmd4, "DD");

                }


    //   cout << cmd1  << " ---  " << cmd2 << endl;
    if (comok == true)
            {
            if(talk1 == true)
                    {
                    puppet(cmd1,comok, LS);
                    sleep(0.75);
                    }
            else
                if(talk2 == true)
                        {
                        puppet(cmd2,comok,LS);
                        }
                else
                    if(talk3 == true)
                            {
                            sleep(4.0);
                            puppet(cmd3,comok,LS);
                            }
                    else
                        if(talk4 == true)
                                {
                                sleep(0.75);
                                puppet(cmd4,comok,LS);
                                }
            }


    }

// ----------------------------------------------

void getRandomchar(char* c, string l)
    {
    struct timeval time;
    gettimeofday(&time,NULL);
    // microsecond has 1 000 000 s
    // Assuming you did not need quite that accuracy
    // Also do not assume the system clock has that accuracy.
    srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
    // The trouble here is that the seed will repeat every
    // 24 days or so.
    // If you use 100 (rather than 1000) the seed repeats every 248 days.
    // Do not make the MISTAKE of using just the tv_usec
    // This will mean your seed repeats every second.
    c[0] = l[rand() % (l.size() - 1)];
    //  cout << c << "  " << rand() % (sizeof(l) - 1) << endl;

    }




void emotion(char* cmd,bool emodir)
    {

    //  string test = "abcdefghijklmnopqrstuvwxyz";
    //  emotions =
    //   int compteur;
    //   compteur= 0;
    //   while(compteur < 20){
    //     getRandomchar(emotions, compteur);
    // //    sleep(1);
    //     compteur+=1;
    //   }
    // all the possible songs
    string emotions =
        "??jp?s?????t??????x??y???z?????????";  // ? mean a random song
    // al the possible moves
    string directions = "gdra12012" ;  //
    // building the command for te puppet function

    string possibilities;
    if (emodir == 0)
            {
            possibilities = emotions;
            }
    else
        if (emodir == 1)
                {
                possibilities = directions;
                }
        else
                {
                log("BUG emotions",1);
                possibilities = "    ";

                }

    //  char cmd[3];
    char c[1];
    getRandomchar(c, possibilities);
    cmd[1] = c[0];
    cmd[2] = '\0';
    //  return cmd;

    }

