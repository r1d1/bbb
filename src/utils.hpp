/* This file was automatically generated.  Do not edit! */
void emotion(char* cmd, bool emodir);
void getRandomchar(const char* c, std::string l);
void brain(cv::Vector<int>res,int xmed,double scale,bool comok,
           serialib& LS, const std::string mode_brain);
int listen(bool comok,serialib& LS, int to );
bool serialevent(bool comok, serialib& LS);
bool puppet(char *cmd,bool comok,serialib& LS);
void log(const std::string&text, bool carriage);
void log_int(const int&text, bool carriage);
void log_serial(const std::string&text);
