/*!
 \file      r1d1.cpp

 note:    The serial program is inspired by the work of Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\note:    The tracking code is inspired from https://code.ros.org/trac/opencv/browser/trunk/opencv/samples/cpp/camshiftdemo.cpp?rev=4234
\author   jnanar
 I place this piece of code in the public domain. Feel free to use as you see
 fit. I'd appreciate it if you keep my name at the top of the code somehwere,
 but whatever.
 Main project site: https://gitorious.org/r1d1
 \version   0.1
 \date      11/04/2015
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "common.h"
#include "depend.hpp"
#include "utils.hpp"



using namespace std;
using namespace cv;

int changemod(int mode, bool comok, serialib& LS)
    {
    if (mode == 0)
            {
            char init[3];
            // pas encore dans le mode face
            strcpy(init, "mm");
            puppet(init,comok,LS);
            strcpy(init, "ff");
            puppet(init,comok,LS);  // sing the song of the face
            mode = 1;
            }
    return mode;
    }

vector<int> detection(Mat& smallImg, Mat& img, CascadeClassifier& cascade,
                      double scale, Scalar color
                     )
    {
    vector<Rect> v;
    cascade.detectMultiScale( smallImg,v,
                              1.1,4,0
                              |CV_HAAR_FIND_BIGGEST_OBJECT,
                              Size(30,30));   // 10,10 OK for paml pretty far - ko for fist
    // 30,30 ok for fist (up in the air) - ko for palm
    int ac = 0; // ac is area of current element
    vector<int> results(6); // five ints with value 0
    if (v.size())
            {
            for(unsigned int i=0; i < v.size(); i++ )
                    {

                    Point pt1(v[i].x + v[i].width/2, v[i].y + v[i].height/2);
                    Point pt2(v[i].x, v[i].y);
                    pt1*= scale;
                    pt2*= scale;
                    //      int r = v[i].width/2 * scale;  // circle radius

                    if(v.size())
                            {
                            //  circle(img, pt1, r, color, 2, 8 );
                            // The rectangle is the zone to track
                            rectangle( img,
                                       Point(v[i].x , v[i].y )*scale,
                                       Point(v[i].x + v[i].width, v[i].y + v[i].height)*scale,
                                       color,
                                       2,  // negative = filled
                                       8 );  // connected lines



                            ac = (v[i].width * v[i].height);  // No Scale here
                            results.at(0) = (v[i].x);
                            results.at(1) =(v[i].width);
                            results.at(2) =(v[i].y);
                            results.at(3) =(v[i].height);
                            results.at(4) =(ac);
                            results.at(5) = (1);

                            }
                    //  pt1*= scale;
                    //  pt2*= scale;
                    }
            }
    return results;
    }

long int detectAndDraw( Mat& img,
                        CascadeClassifier& cascade_face,
                        CascadeClassifier& cascade_palm,
                        CascadeClassifier& cascade_fist,
                        double scale,
                        bool comok, bool win,
                        time_t timer1,serialib& LS,
                        Rect& selection, int& trackObject
                      )
    {
    //    cout << "Detection - " ;
    Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale),
                        CV_8UC1 );
    cvtColor( img, gray, CV_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );

    int ac = 0; // ac is area of current element
    vector<int> res ;
    res = detection( smallImg, img, cascade_fist,
                     scale, Scalar( 0, 255, 255 ) );
    if (res.at(4) < 1)
            {
            // a face is detected, we have a match
            res = detection(smallImg, img, cascade_face,
                            scale, Scalar(255,0,0));
            }

    //     res = detection(smallImg, img, cascade_face,
    //          scale, Scalar(0,125,125),win );

    ac = res.at(4) * scale*scale;
    int centerX = res.at(1)/2;
    centerX += res.at(0);
    int centerY = res.at(3)/2;
    centerY += res.at(2);
    centerX *=scale; // reapply the scale factor.
    centerY *=scale;

    selection = Rect(res.at(0)*scale,res.at(1)*scale,res.at(2)*scale,
                     res.at(3)*scale);

    // This part is OK, we have the center of the picture
    Point pt(centerX, centerY);
    circle(img, pt, 3, cvScalar(0,255 , 0, 0), 2, 8, 0);  // vert
    time_t timer2;
    timer2 = time(0);
    bool roger = false;


    char cmd[3];
    if (difftime(timer2,timer1) > 25 && difftime(timer2,timer1) < 45
            && checkpoint == 0 && (mode ==1))
            {
            if (debug_mode == 1)
                    {
                    cout <<"Timer " << "25 < t < 45"<< " "<< "checkpoint " << checkpoint <<  endl;
                    cout << "Timer " << difftime(timer2,timer1) << endl;
                    }
            checkpoint = 1 ;
            log("No face for 25 seconds ",0);
            emotion(cmd,0); // emotion
            puppet(cmd,comok, LS);  // Letter for emotion
            sleep(1);
            emotion(cmd,1); // direction
            puppet(cmd,comok, LS);// Letter for direction
            log("search for faces",1);


            }
    else
        if (difftime(timer2,timer1) > 45 && difftime(timer2,timer1) < 100
                && checkpoint ==  1 &&  (mode ==1))
                {
                // no face since 5 seconds, have to scan around to see if theire is one and make some sounds
                if (debug_mode == 1)
                        {
                        cout << "Timer " << "t > 45" << " "<< "checkpoint " << checkpoint <<  endl;
                        cout << "Timer " << difftime(timer2,timer1) << endl;
                        }
                checkpoint = 2 ;
                log("No face for 45 seconds ",0);
                emotion(cmd,0) ; // emotion
                puppet(cmd,comok, LS);  // Letter for emotion
                sleep(1);
                emotion(cmd,1); // direction
                puppet(cmd,comok, LS);// Letter for direction
                log("search for faces",1);
                mode = 0;  // play a song when you have another face because it's a long time since you saw a face
                }
        else
            if (difftime(timer2,timer1) > 100
                    && checkpoint == 2)   // no face since 100 seconds
                    {
                    checkpoint = 3;

                    if (debug_mode == 1)
                            {

                            cout << "checkpoint " << checkpoint <<  endl;
                            cout << "Timer " << "t > 100" << " "<< "checkpoint " << checkpoint <<  endl;
                            cout << "Timer " << difftime(timer2,timer1) << endl;
                            }

                    mode = 0;  // exploration mode while mode is < 0; not really a face
                    facedetect = 0;
                    if (comok == true)
                            {
                            strcpy(cmd, "tt");
                            puppet(cmd,comok, LS);

                            strcpy(cmd, "ee");
                            puppet(cmd,comok, LS);
                            strcpy(cmd, "ee");
                            puppet(cmd,comok, LS);
                            strcpy(cmd, "ee");
                            puppet(cmd,comok, LS);
                            }
                    //    cout << "LOST THE FACE" << endl;
                    log("No face for 100 seconds ",0);
                    log("Lost the face",1);
                    //    cout << "------------------------" << endl;
                    }
    // we have some faces
    if (ac != 0 || centerX != 0 || centerY !=0 )
            {
            checkpoint = 0 ;
            if (facedetect >= 0 || mode == 0 )
                    {
                    strcpy(cmd, "mm"); // send mm every 20 times to be sure arduino does not forget
                    puppet(cmd, comok,LS);
                    puppet(cmd, comok,LS); // be sure to send the m function
                    puppet(cmd, comok,LS);
                    facedetect = 0;
                    }
            facedetect+=1;
            if (mode == 0)
                    {
                    roger = serialevent(comok,LS );  // get the serial messages from the arduino
                    if (roger == 1)
                            {
                            log("Got the face",1);
                            strcpy(cmd, "jj");
                            puppet(cmd ,comok, LS);
                            mode = changemod(mode, comok,LS);
                            }
                    }
            if (debug_mode == 1)
                    {
                    cout << "facedetect " << facedetect << endl;

                    cout <<"-----------------------------------------------" << endl;
                    cout << centerX << "  "<< centerY << "  "<< ac << endl;
                    cout << img.cols/2 << "  "<< img.rows/2 << "  "<< 0.9*img.cols/2 << endl;
                    }

            // detection ! timer = now
            std::ofstream log_file(
                "log.txt", std::ios_base::out | std::ios_base::app );
            log_file << "Area: " << ac << endl;
            log_file << "x: " << res[0]+ (res[1]/2)<< endl;
            log_file << "y: " << res[2]+(res[3]/2) << endl;
            timer1 = time(0);
            trackObject = -1;
            }


    if (comok == true && facedetect > 0 && ac != 0)
            {
            int xmed ; //, ymed;
            xmed = img.cols/2;   // center of the picture
            //      ymed = img.rows/2;
            brain(res, xmed, scale ,comok,LS, "detect");


            }
    if (win == true)
            {
            cv::imshow( "result", img );
            }

    return timer1;
    }



