CC=g++
CFLAGS=-Wall -Wextra -pedantic -Wunreachable-code
LDFLAGS=-O2 `pkg-config --cflags --libs opencv`
EXECUTABLE=r1d1
BD=obj
OBJS= $(BD)/ini.o $(BD)/INIReader.o $(BD)/serialib.o $(BD)/utils.o $(BD)/detect.o  $(BD)/tracking.o  $(BD)/main.o

all: executable

default: all

Debug: CXX += -DDEBUG -g
Debug: CC += -DDEBUG -g
Debug: eDebug

Release: all

executable :
	(cd src/lib ; make)
	(cd src ; make)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -o $(EXECUTABLE)

eDebug :
	(cd src/lib ; make debug)
	(cd src ; make debug)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -o $(EXECUTABLE)
	(mv r1d1 bin/Debug/)



clean:
	rm -rf $(BD)/*o  $(EXECUTABLE)

